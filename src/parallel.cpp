//
// Created by Amadeus on 2023/8/11.
//


#include "parallel.h"

using namespace pg;


Semaphore::Semaphore(int count) : count_(count) {}

void Semaphore::notify() {
    std::unique_lock<std::mutex> lock(mutex_);
    ++count_;
    cv_.notify_one();
}

void Semaphore::wait() {
    std::unique_lock<std::mutex> lock(mutex_);
    cv_.wait(lock, [this] { return count_ > 0; });
    --count_;
}

ThreadPool::ThreadPool(int n) {
    for (int i = 0; i < n; ++i) {
        threads.emplace_back(
                [&]() {
                    while (true) {
                        std::unique_lock<std::mutex> lk(mtx);
                        cv.wait(lk, [&]() {
                            return stop || !tasks.empty();
                        });
                        if (!tasks.empty()) {
                            auto t = tasks.front();
                            tasks.pop();
                            lk.unlock();
                            t();
                        } else if (stop) {
                            lk.unlock();
                            return;
                        }
                    }
                }
        );
    }
}

bool ThreadPool::submit(const std::function<int()> &t) {
    std::lock_guard<std::mutex> lk(mtx);
    tasks.push(t);
    cv.notify_one();
    return true;
}

ThreadPool::~ThreadPool() {
    stop = true;
    cv.notify_all();
    // 等待线程退出
    for (auto &th: threads) {
        th.join();
    }
}
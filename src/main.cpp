#include <iostream>
//#include "auto_ptr.h"
#include <memory>

struct B;


struct B {
    static int num;

    B() {
        ++num;
    }
};

class A {
public:
    static int num;
};

int B::num = 0;

int A::num = 10;

int main() {
    std::shared_ptr<B> b1;
    auto b3 = new B();
    std::shared_ptr<B> b2(b1, b3);
    std::cout << b2->num << std::endl;
    return 0;
}

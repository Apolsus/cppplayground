//
// Created by Amadeus on 2023/8/5.
//

#ifndef PLAYGROUND_AUTO_PTR_H
#define PLAYGROUND_AUTO_PTR_H

#include <atomic>
#include <stdexcept>

namespace pg {

    struct _control_block {
        /* The reason for the existence of this pointer is complex,
         * see  https://stackoverflow.com/questions/34046070/why-are-two-raw-pointers-to-the-managed-object-needed-in-stdshared-ptr-impleme#comment55847655_34046070*/
        int *ptr;
        std::atomic<int> real;
        std::atomic<int> weak;
    };

    class weak_ptr;

    class shared_ptr;

    class shared_ptr {
        friend weak_ptr;
        friend shared_ptr make_shared(int a);
        _control_block *ctrl;
        int *ptr;

        void release();

    public:

        explicit shared_ptr(int *ptr = nullptr);

        shared_ptr(const pg::shared_ptr &other, int *ptr) noexcept;

        shared_ptr(pg::shared_ptr &&other, int *ptr) noexcept;


        shared_ptr(const shared_ptr &other) noexcept;


        shared_ptr(shared_ptr &&other) noexcept;

        explicit shared_ptr(const weak_ptr &other) ;

        shared_ptr &operator=(const shared_ptr &other) noexcept;

        shared_ptr &operator=(shared_ptr &&other) noexcept;

        ~shared_ptr();

        int operator*() const;

        int *operator->() const;

        int *get() const; //NOTE: use for compatibility

        int use_count() const;

        explicit operator bool() const;

    };

    class weak_ptr {
        friend class shared_ptr;

        _control_block *ctrl{};
        int *ptr{};

        void release();

    public:
        weak_ptr();

        explicit weak_ptr(const shared_ptr &other) noexcept;

        weak_ptr(const weak_ptr &other) noexcept;

        weak_ptr(weak_ptr &&other) noexcept;

        weak_ptr &operator=(const weak_ptr &other) noexcept;

        weak_ptr &operator=(weak_ptr &&other) noexcept;

        //NOTE: return value is discard-able,
        // because share_ptr auto manage memory
        shared_ptr lock() const;

        ~weak_ptr();

        int use_count() const;

        bool expired() const;
    };

    class unique_ptr {
        const int *ptr;
    public:
        explicit unique_ptr(const int *ptr = nullptr);

        unique_ptr(const unique_ptr &other) = delete;

        unique_ptr(unique_ptr &&other) noexcept;

        unique_ptr &operator=(const unique_ptr &other) = delete;

        unique_ptr &operator=(unique_ptr &&other) noexcept;

        ~unique_ptr();

        int operator*() const;

        int *operator->() const;

        int *get() const;

        explicit operator bool() const;
    };

    shared_ptr make_shared(int a);

    unique_ptr make_unique(int a);
}

#endif //PLAYGROUND_AUTO_PTR_H

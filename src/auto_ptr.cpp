//
// Created by Amadeus on 2023/8/5.
//

#include "auto_ptr.h"
#include <new>
#include <utility>

using namespace pg;


shared_ptr::shared_ptr(int *ptr) {
    this->ptr = ptr;
    if (ptr == nullptr) {
        ctrl = nullptr;
        return;
    }
    ctrl = new _control_block{ptr, 1, 0};
}

shared_ptr::shared_ptr(const pg::shared_ptr &other, int *ptr) noexcept {
    this->ptr = ptr;
    ctrl = other.ctrl;
    if (ctrl != nullptr) {
        ctrl->real++;
    }
}

shared_ptr::shared_ptr(pg::shared_ptr &&other, int *ptr) noexcept {
    this->ptr = ptr;
    ctrl = other.ctrl;
    other.ctrl = nullptr;
    other.ptr = nullptr;
}

shared_ptr::shared_ptr(const pg::shared_ptr &other) noexcept {
    ptr = other.ptr;
    ctrl = other.ctrl;
    if (ctrl != nullptr) {
        ctrl->real++;
    }
}

shared_ptr::shared_ptr(pg::shared_ptr &&other) noexcept {
    ptr = other.ptr;
    ctrl = other.ctrl;
    other.ptr = nullptr;
    other.ctrl = nullptr;
}

shared_ptr &shared_ptr::operator=(const shared_ptr &other) noexcept {
    if (this == &other) {
        return *this;
    }
    this->release();
    ptr = other.ptr;
    ctrl = other.ctrl;
    if (ctrl != nullptr) {
        ctrl->real++;
    }
    return *this;
}

shared_ptr &shared_ptr::operator=(pg::shared_ptr &&other) noexcept {
    if (this == &other) {
        return *this;
    }
    this->release();
    ptr = other.ptr;
    ctrl = other.ctrl;
    other.ptr = nullptr;
    other.ctrl = nullptr;
    return *this;
}

shared_ptr::shared_ptr(const pg::weak_ptr &other) {
    ptr = other.ptr;
    ctrl = other.ctrl;
    if (ctrl != nullptr) {
        if (ctrl->real != 0) {
            ctrl->real++;
        } else {
            ctrl = nullptr;
        }
    }
}

void shared_ptr::release() {
    if (ctrl == nullptr) {
        return;
    }
    ctrl->real--;
    if (ctrl->real == 0) {
        delete ctrl->ptr;
        ctrl->ptr = nullptr;
        ptr = nullptr;
        if (ctrl->weak == 0) {
            delete ctrl;
            ctrl = nullptr;
        }
    }
}

shared_ptr::~shared_ptr() {
    this->release();
}

int shared_ptr::operator*() const {
    return *ptr;
}

int *shared_ptr::operator->() const {
    return ptr;
}

int *shared_ptr::get() const {
    return ptr;
}

int shared_ptr::use_count() const {
    if (ctrl == nullptr) {
        return 0;
    }
    return ctrl->real;
}

shared_ptr::operator bool() const {
    return ctrl != nullptr;
}


weak_ptr::weak_ptr(const shared_ptr &other) noexcept {
    ptr = other.ptr;
    ctrl = other.ctrl;
    if (ctrl != nullptr) {
        ctrl->weak++;
    }
}

void weak_ptr::release() {
    if (ctrl == nullptr) {
        return;
    }
    ctrl->weak--;
    if (ctrl->weak == 0 && ctrl->real == 0) {
        delete ctrl;
    }
    ctrl = nullptr;
}

weak_ptr::weak_ptr() {
    ctrl = nullptr;
    ptr = nullptr;
};

weak_ptr::weak_ptr(const pg::weak_ptr &other) noexcept {
    ptr = other.ptr;
    ctrl = other.ctrl;
    if (ctrl != nullptr) {
        ctrl->weak++;
    }
}

weak_ptr::weak_ptr(pg::weak_ptr &&other) noexcept {
    ptr = other.ptr;
    ctrl = other.ctrl;
    other.ctrl = nullptr;
    other.ptr = nullptr;
}

weak_ptr &weak_ptr::operator=(const pg::weak_ptr &other) noexcept {
    if (this == &other) {
        return *this;
    }
    this->release();
    ptr = other.ptr;
    ctrl = other.ctrl;
    if (ctrl != nullptr) {
        ctrl->weak++;
    }
    return *this;
}

weak_ptr &weak_ptr::operator=(pg::weak_ptr &&other) noexcept {
    if (this == &other) {
        return *this;
    }
    this->release();
    ptr = other.ptr;
    ctrl = other.ctrl;
    other.ptr = nullptr;
    other.ctrl = nullptr;
    return *this;
}

weak_ptr::~weak_ptr() {
    this->release();
}

shared_ptr weak_ptr::lock() const {
    return shared_ptr(*this);
}

int weak_ptr::use_count() const {
    if (ctrl == nullptr) {
        return 0;
    }
    return ctrl->weak;
}

bool weak_ptr::expired() const {
    return ctrl == nullptr || ctrl->real == 0;
}


unique_ptr::unique_ptr(const int *ptr) : ptr(ptr) {}

unique_ptr::unique_ptr(pg::unique_ptr &&other) noexcept {
    ptr = other.ptr;
    other.ptr = nullptr;
}

unique_ptr &unique_ptr::operator=(pg::unique_ptr &&other) noexcept {
    if (this == &other) {
        return *this;
    }
    delete ptr;
    ptr = other.ptr;
    other.ptr = nullptr;
    return *this;
}

unique_ptr::~unique_ptr() {
    delete ptr;
}

int unique_ptr::operator*() const {
    if (ptr == nullptr) {
        throw std::runtime_error("nullptr");
    }
    return *ptr;
}

int *unique_ptr::operator->() const {
    if (ptr == nullptr) {
        throw std::runtime_error("nullptr");
    }
    return const_cast<int *>(ptr);
}

int *unique_ptr::get() const {
    return const_cast<int *>(ptr);
}

unique_ptr::operator bool() const {
    return ptr != nullptr;
}

struct make_shared_helper : _control_block {
    int value;
};


shared_ptr pg::make_shared(int a) {
    //NOTE: 实际上，make_share 的好处是只一次内存分配就把控制块和对象分配出来
    shared_ptr ptr;
    auto *ctrl = new make_shared_helper();
    ptr.ptr = new(&ctrl) int(a);
    ptr.ctrl = new(&ctrl + sizeof(int)) _control_block{ptr.ptr, 1, 0};
    return std::move(ptr);
}

unique_ptr make_unique(int a) {
    return unique_ptr(new int(a));
}


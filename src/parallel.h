//
// Created by Amadeus on 2023/8/11.
//

#ifndef PLAYGROUND_PARALLEL_H
#define PLAYGROUND_PARALLEL_H

#include <thread>
#include <queue>

namespace pg {
    class Semaphore {
    private:
        std::mutex mutex_;
        std::condition_variable cv_;
        int count_;
    public:
        explicit Semaphore(int count = 0);

        void notify();

        void wait();
    };

    class ThreadPool {
    private:
        std::mutex mtx;
        std::queue<std::function<int()>> tasks;
        std::vector<std::thread> threads;
        std::atomic<bool> stop = false;
        std::condition_variable cv;

    public:
        explicit ThreadPool(int n);

        bool submit(const std::function<int()> &t);

        ~ThreadPool();
    };
}

#endif //PLAYGROUND_PARALLEL_H
